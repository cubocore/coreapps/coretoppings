/*
    *
    * This file is a part CoreAction
    * pactlqt is a Qt based UI to perform pulseaudio volume control
    * Copyright 2019 CuboCore Group
    *

    *
    * ============================== DISCLAIMER ==============================
    *
    * This is intended to be a plugin for CoreAction from the CuboCore Suite.
    * However, this plugin is NOT a part of CoreApps or not related to
    * CuboCore in anyway.
    *
    * I am providing this purely as a third-party plugin, and all bugs are to
    * be reported at https://gitlab.com/marcusbritanicus/pactlqt/issues only.
    *
    * ========================================================================
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QTimerEvent>
#include <QBasicTimer>
#include <QSlider>
#include <QToolButton>
#include <QtPlugin>

#include <cprime/cplugininterface.h>

class PactlQtDevice {

	public:
		PactlQtDevice( QString name, int id );

		QString name();

		int currentVolume();
		bool isMuted();

		void setVolume( int );
		void toggleMute();

	private:
		int mDeviceId;
		QString mName;
};

typedef QList<PactlQtDevice> PactlQtDevices;

class PactlQtWidget : public QWidget {

    Q_OBJECT

	public:
		PactlQtWidget(QWidget *parent);

		void refresh();

	private:
		QList<QSlider *> sliders;
		QList<QToolButton *> muteBtns;
		PactlQtDevices PactlQts;

		int maxBrightness, curBrightness;

		QBasicTimer timer;
		int changedValue = -1;

		void setCurrent();

	private Q_SLOTS:
		void changeVolume( int );
		void toggleMuteVolume();

	protected:
		void timerEvent(QTimerEvent *);
};

class PactlQtPlugin : public WidgetsInterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID WIDGETS_PLUGININTERFACE)
    Q_INTERFACES(WidgetsInterface)

	public:
		/* Name of the plugin */
		QString name();

		/* The plugin version */
		QString version();

		/* The Widget */
		QWidget *widget(QWidget *);
};
