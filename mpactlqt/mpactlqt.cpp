/*
    *
    * This file is a part of CoreAction.
    * pactlqt is a Qt based UI to perform pulseaudio volume control.
    * Copyright 2019 CuboCore Group
    *

    *
    * ============================== DISCLAIMER ==============================
    *
    * This is intended to be a plugin for CoreAction from the CuboCore Suite.
    * However, this plugin is NOT a part of CoreApps or not related to
    * CuboCore in anyway.
    *
    * I am providing this purely as a third-party plugin, and all bugs are to
    * be reported at https://gitlab.com/marcusbritanicus/pactlqt/issues only.
    *
    * ========================================================================
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <cprime/messageengine.h>

#include "mpactlqt.hpp"


PactlQtDevice::PactlQtDevice( QString name, int id ) {

	mDeviceId = id;
	mName = name;
};

QString PactlQtDevice::name() {

	return mName;
}

int PactlQtDevice::currentVolume() {

	QProcess proc;
    proc.start( "pactl", QStringList() << "list" << "sources" );
	proc.waitForFinished();

	QStringList data = QString::fromLocal8Bit( proc.readAll() ).split( "\n", Qt::SkipEmptyParts );
	Q_FOREACH( QString chunk, data ) {
		if ( chunk.simplified().trimmed().startsWith( "Volume" ) ) {
			QStringList chunks = chunk.simplified().trimmed().split( " ", Qt::SkipEmptyParts );
			int left = chunks.value( 4, "-1" ).replace( "%", "" ).toInt();
			int right = chunks.value( 11, "-1" ).replace( "%", "" ).toInt();

			if ( ( left >= 0 ) and ( right >= 0 ) )
                return static_cast<int>( left + right ) / 2;
		}
	}

	return 0;
};

bool PactlQtDevice::isMuted() {

	QProcess proc;
    proc.start( "pactl", QStringList() << "list" << "sources" );
	proc.waitForFinished();

	QStringList data = QString::fromLocal8Bit( proc.readAll() ).split( "\n", Qt::SkipEmptyParts );
	Q_FOREACH( QString chunk, data ) {
		if ( chunk.simplified().trimmed().startsWith( "Mute:" ) ) {
			QStringList chunks = chunk.simplified().trimmed().split( " ", Qt::SkipEmptyParts );

			if ( chunks.value( 1 ) == "yes" )
				return true;

			else
				return false;
		}
	}

	/* By default, treat it as muted */
	return true;
};

void PactlQtDevice::setVolume( int volume ) {

	QProcess proc;
    proc.startDetached( "pactl", QStringList() << "set-source-volume" << QString::number( mDeviceId ) << QString( "%1%" ).arg( volume ) );
};

void PactlQtDevice::toggleMute() {

	QProcess proc;
    proc.startDetached( "pactl", QStringList() << "set-source-mute" << QString::number( mDeviceId ) << QString( "toggle" ) );
};

PactlQtWidget::PactlQtWidget( QWidget *parent ) : QWidget( parent ) {

	QProcess proc;
    proc.start( "pactl", QStringList() << "list" << "short" << "sources" );
	proc.waitForFinished();

	int devices = QString::fromLocal8Bit( proc.readAll() ).split( "\n", Qt::SkipEmptyParts ).size();

    proc.start( "pactl", QStringList() << "list" << "sources" );
	proc.waitForFinished();

	QStringList devProps = QString::fromLocal8Bit( proc.readAll() ).split( "\n\n", Qt::SkipEmptyParts );

	for( int i = 0; i < devices; i++ ) {

		// To get the name of the device
		QStringList props = devProps.value( i ).split( "\n", Qt::SkipEmptyParts );
		QString name;
		Q_FOREACH( QString prop, props ) {
			if ( prop.trimmed().simplified().startsWith( "device.description" ) ) {
				name = prop.trimmed().simplified().replace( "\"", "" ).split( " = ", Qt::SkipEmptyParts ).value( 1 );
				break;
			}
		}

		PactlQtDevice dev( name, i );
		PactlQts << dev;

		QSlider *slider = new QSlider( Qt::Horizontal, this );
		slider->setRange( 0, 100 );
		connect(slider, &QSlider::sliderReleased, [=]() {
			changeVolume(slider->value());
		});
//		connect( slider, SIGNAL( sliderMoved( int ) ), this, SLOT( changeVolume( int ) ) );
		connect( slider, SIGNAL( valueChanged( int ) ), this, SLOT( changeVolume( int ) ) );

		QToolButton *muteBtn = new QToolButton();
		muteBtn->setFixedSize( QSize( 32, 32 ) );
        muteBtn->setIcon( QIcon::fromTheme( "microphone-sensitivity-muted" ) );
		muteBtn->setCheckable( true );
		muteBtn->setAutoRaise( true );
		connect( muteBtn, SIGNAL( clicked() ), this, SLOT( toggleMuteVolume() ) );

		if ( dev.isMuted() ) {
			slider->setDisabled( true );
			muteBtn->setChecked( true );
		}

		sliders << slider;
		muteBtns << muteBtn;
	}

	timer.start( 1000, this );                     // 1 sec timer. Checks if any other process has changed the volume behind the scenes.

	refresh();
}

void PactlQtWidget::refresh() {

	// qDeleteAll( children() );

    QLabel *lbl = new QLabel( "MIC VOLUME CONTROL" );
	lbl->setFont( QFont( font().family(), 11 ) );
    lbl->setAlignment( Qt::AlignLeft );

	QGridLayout *lyt = new QGridLayout();
	lyt->setAlignment( Qt::AlignCenter );

	lyt->addWidget( lbl, 0, 0, 1, 2, Qt::AlignCenter );

	for ( int i = 0; i < PactlQts.count(); i++ ) {
		lyt->addWidget( new QLabel( PactlQts[ i ].name() ), i * 2 + 1, 0, 1, 2 );
		lyt->addWidget( sliders[ i ], i * 2 + 2, 0 );
		lyt->addWidget( muteBtns[ i ], i * 2 + 2, 1 );
	}

	setLayout( lyt );

	setCurrent();
};

void PactlQtWidget::setCurrent() {

	for (int i = 0; i < PactlQts.count(); i++) {

		/* Don't change the value of a slider which is being changed */
		if ( sliders[ i ]->isSliderDown() ) {
			continue;
		}

		int volume = PactlQts[ i ].currentVolume();

		if ( volume == -1 ) {
            CPrime::MessageEngine::showMessage(
						"cc.cubocore.CoreAction",
						"CoreAction",
                        "Somthing Wrong",
                        "Unable to get the current volume of the device. Default value taken as 100." );
			sliders[ i ]->setValue( 100 );

			continue;
		}

		sliders[ i ]->setValue( volume );
	}
}

void PactlQtWidget::changeVolume( int value ) {

	/* Get the index of the slider which was moved */
	QSlider *slider = qobject_cast<QSlider *>(sender());

	int sliderIndex = -1;

	if ( slider ) {
		sliderIndex = sliders.indexOf( slider );

		if (slider->isSliderDown()) {
			changedValue = value;
			return;
		} else {
			if (changedValue >= 0) {
				slider->setValue(changedValue);
				changedValue = -1;
			}
		}

		PactlQts[ sliderIndex ].setVolume( slider->value() );
	}
}

void PactlQtWidget::toggleMuteVolume() {

	/* Get the index of the slider which was moved */
	QToolButton *btn = qobject_cast<QToolButton *>( sender() );

	int btnIndex = -1;

	if ( btn ) {
		btnIndex = muteBtns.indexOf( btn );
		if ( btn->isChecked() )
			sliders[ btnIndex ]->setDisabled( true );

		else
			sliders[ btnIndex ]->setEnabled( true );

		PactlQts[ btnIndex ].toggleMute();
	}
}

void PactlQtWidget::timerEvent(QTimerEvent *tEvent) {

	if (tEvent->timerId() == timer.timerId()) {
		/* Set the actual position of the slider */

		setCurrent();
	}

	QWidget::timerEvent(tEvent);
	tEvent->accept();
}

/* Name of the plugin */
QString PactlQtPlugin::name() {

    return "Mic Volume Control";
}

/* The plugin version */
QString PactlQtPlugin::version() {

    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *PactlQtPlugin::widget( QWidget *parent ) {

	return new PactlQtWidget( parent );
}
