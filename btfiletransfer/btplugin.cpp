/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include "remoteselector.h"
#include "btplugin.h"

QString BTSharePlugin::name() {

	return "Bluetooth";
};

QIcon BTSharePlugin::icon() {

	return QIcon::fromTheme( "bluetooth" );
};

QStringList BTSharePlugin::mimeTypes() {

	return QStringList() << "*";
};

ShareItInterface::Context BTSharePlugin::context() {

	return ShareItInterface::Files;
};

bool BTSharePlugin::shareItDialog( QStringList files, QWidget *parent ) {

	QBluetoothLocalDevice *dev = new QBluetoothLocalDevice( nullptr );
	if ( dev->hostMode() == QBluetoothLocalDevice::HostPoweredOff ) {
		QMessageBox::warning(
			parent,
			"Bluetooth powered off",
			"Your bluetooth adapter is powered off. Please switch it on and try again."
		);

		return false;
	}

    RemoteSelector *bt = new RemoteSelector( files, parent );
    bt->startDiscovery();

    return bt->exec();
};
