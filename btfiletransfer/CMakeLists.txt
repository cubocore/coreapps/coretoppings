project( btshare )
cmake_minimum_required( VERSION 3.16 )

set( PROJECT_VERSION 5.0.0 )
set( PROJECT_VERSION_MAJOR 5 )
set( PROJECT_VERSION_MINOR 0 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )
add_compile_definitions(VERSION_TEXT="${PROJECT_VERSION}")

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )
set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_AUTOUIC ON )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

find_package( Qt6Core REQUIRED )
find_package( Qt6Gui REQUIRED )
find_package( Qt6Widgets REQUIRED )
find_package( Qt6Bluetooth REQUIRED )

set( btshare_HDRS

)

set( btshare_SRCS
	remoteselector.cpp
	progress.cpp
	pindisplay.cpp
	btplugin.cpp
)

set( btshare_MOCS
	remoteselector.h
	progress.h
	pindisplay.h
	btplugin.h
)

set( btshare_UIS
	remoteselector.ui
	progress.ui
	pindisplay.ui
)

set ( btshare_RSCS
	btfiletransfer.qrc
)

add_library( btshare MODULE ${btshare_SRCS} ${btshare_MOCS} ${btshare_UIS} ${btshare_RSCS}  )
target_link_libraries( btshare  Qt6::Core Qt6::Gui Qt6::Widgets Qt6::Bluetooth  cprime-widgets cprime-core )

install( TARGETS btshare LIBRARY DESTINATION lib/coreapps/shareit )
