#pragma once

#include <QtCore>

struct IconPixmap {
    int width;
    int height;
    QByteArray bytes;
};

typedef QList<IconPixmap> IconPixmapList;

struct ToolTip {
    QString iconName;
    QList<IconPixmap> iconPixmap;
    QString title;
    QString description;
};

Q_DECLARE_METATYPE(IconPixmapList);
Q_DECLARE_METATYPE(IconPixmap);
Q_DECLARE_METATYPE(ToolTip);
