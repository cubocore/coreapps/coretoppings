/*
    *
    * This file is a part of CorePKit.
    * PolicyKit based helper for performing CoreApps actions with elevated privileges.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QtCore>

int main( int argc, char *argv[] ) {

	QCoreApplication app( argc, argv );

	app.setApplicationName( "CorePKit" );
	app.setOrganizationName( "CoreApps" );
    app.setApplicationVersion( "4.2.0" );

	QCommandLineParser cliparser;
	cliparser.addHelpOption();         // Help
    cliparser.addVersionOption();      // Version

	// Description
	cliparser.setApplicationDescription( "PolicyKit based helper for performing CoreApps actions with elevated privileges" );

	cliparser.addOption( { "brightness", "Set the brightness of the lcd in percentage.", "brightness" } );
	cliparser.addOption( { "increase-brightness", "Increase the brightness of the lcd by 5%" } );
	cliparser.addOption( { "decrease-brightness", "Increase the brightness of the lcd by 5%" } );
	cliparser.addPositionalArgument( "file", "The file to which the brightness is written." );

	cliparser.process(app);

	if ( cliparser.isSet( "brightness" ) ) {
		if ( cliparser.positionalArguments().count() < 1 ) {
			qDebug() << "Specify the brightness file";
			return 128;
		}

		qDebug() << "Setting brightness to" << cliparser.value( "brightness" );

		QFile backlight( cliparser.positionalArguments().at( 0 ) );
		backlight.open( QFile::WriteOnly );
		backlight.write( cliparser.value( "brightness" ).toUtf8() );
		backlight.close();
	}
};
