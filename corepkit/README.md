# CorePKit
 PolicyKit based helper for performing CoreApps actions with elevated privileges

## Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/corefm/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt6-base
* [libcprime](https://gitlab.com/cubocore/libcprime)
* [libcsys](https://gitlab.com/cubocore/libcsys)

### Information
Please see the [Wiki page](https://gitlab.com/cubocore/wiki) for this info.
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CuboCore. Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).
