/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>

#include <cprime/cplugininterface.h>
#include <QBasicTimer>

QT_BEGIN_NAMESPACE
namespace Ui {
    class playerctlqt;
}
QT_END_NAMESPACE

class playerctlqt : public QWidget {
    Q_OBJECT

public:
    playerctlqt(QWidget *parent = nullptr);
    ~playerctlqt();

protected:
    void timerEvent(QTimerEvent *tEvent);

private slots:
    void get_playing_media();
    void check_status();
    void init();
    void on_toolButton_prev_clicked();
    void on_toolButton_playpause_clicked();
    void on_toolButton_next_clicked();
    void on_toolButton_shuffle_clicked(bool checked);
    void on_toolButton_stop_clicked();

private:
    Ui::playerctlqt *ui;
    QBasicTimer *timer;
};

class playerctlqtPlugin : public WidgetsInterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID WIDGETS_PLUGININTERFACE)
    Q_INTERFACES(WidgetsInterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);
};
