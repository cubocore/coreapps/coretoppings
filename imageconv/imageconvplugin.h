/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <cprime/cplugininterface.h>

class imageconvplugin : public ShareItInterface {
    Q_OBJECT

    Q_PLUGIN_METADATA(IID SHAREIT_PLUGININTERFACE)
    Q_INTERFACES(ShareItInterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QIcon icon();

    /* MimeTypes handled by the plugin */
    QStringList mimeTypes();

    /* Context of the plugin */
	Context context();

    /* The Widget */
    bool shareItDialog(QStringList files, QWidget *parent);
};
