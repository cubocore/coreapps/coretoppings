/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QImageReader>

#include <cprime/filefunc.h>

#include "imageconv.h"
#include "imageconvplugin.h"

QString imageconvplugin::name()
{
    return "Image Conversion";
}

QIcon imageconvplugin::icon()
{
    return QIcon::fromTheme("image-ico");
}

QStringList imageconvplugin::mimeTypes()
{
    QStringList mimeList;

    Q_FOREACH (QByteArray byte, QImageReader::supportedImageFormats()) {
        QString file = QString("file.") + QString::fromLocal8Bit(byte);
        mimeList << CPrime::FileUtils::mimeType(file).name();
	}

    return mimeList;
}

ShareItInterface::Context imageconvplugin::context()
{
    return ShareItInterface::Files;
}

bool imageconvplugin::shareItDialog(QStringList files, QWidget *parent)
{
    imageconv *conv = new imageconv(files, parent);
    return conv->exec();
}
