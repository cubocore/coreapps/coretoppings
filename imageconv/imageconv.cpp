/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QDebug>
#include <QImageReader>
#include <QImageWriter>
#include <QFileInfo>

#include <cprime/messageengine.h>

#include "imageconv.h"
#include "ui_imageconv.h"


imageconv::imageconv(QStringList files, QWidget *parent) :QDialog(parent),ui(new Ui::imageconv),
    mFiles(files)
{
    ui->setupUi(this);

    // TODO
    // Using the first image file if a list of files given

    ui->imgName->setText(QFileInfo(mFiles.at(0)).fileName());

    // Current image type
    QString cType = QFileInfo(mFiles.at(0)).suffix();

    QStringList list;

    Q_FOREACH (QByteArray format, QImageWriter::supportedImageFormats()) {
        QString t = QString::fromStdString(format.toStdString());

        if (t == cType) {
            continue; // Ignore the current selected image type.
        }

        list.append(t);
    }

    ui->types->addItems(list);
}

imageconv::~imageconv()
{
    delete ui;
}

QStringList mimeList()
{
    QStringList mimeTypes;

    Q_FOREACH (QByteArray type, QImageReader::supportedMimeTypes()) {
        mimeTypes.append(QString::fromStdString(type.toStdString()));
    }

    return mimeTypes;
}

void imageconv::on_convert_clicked()
{
    QString file = mFiles.at(0);

    qDebug() << "Selected type " << ui->types->currentText();
    qDebug() << "Full file path " << file;

    QFileInfo fi(file);
    QString prop = fi.path() + "/" + fi.baseName() + "." + ui->types->currentText();

    qDebug() << "Proposed File Name" << prop;

    QImage img(file);
    QImageWriter *imgwr = new QImageWriter(prop);

    if (imgwr->write(img)) {
		CPrime::MessageEngine::showMessage("image-ico", "Image Conversion", "Image converted successfully.", "Image saved at the source directory.");
    } else {
		CPrime::MessageEngine::showMessage("image-ico", "Image Conversion", "Image converted unsuccessfully.", "Something really happened badly.");
    }

    close();
}
