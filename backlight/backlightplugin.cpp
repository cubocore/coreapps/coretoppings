/*
  *
  * This file is a part of CoreAction.
  * A side bar for showing widgets for C Suite.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */

#include <QFile>
#include <QDir>
#include <QVBoxLayout>
#include <QLabel>
#include <QProcess>
#include <QtDBus>

#include <cprime/messageengine.h>

#include "backlightplugin.hpp"


BacklightDevice::BacklightDevice(QString path)
{
	devPath = QString(path);

	QFile curFile(devPath + "/max_brightness");

	maxBrightness = 255.0;

	if (curFile.open(QIODevice::ReadOnly))
	{
		maxBrightness = QString::fromLocal8Bit(curFile.readAll()).simplified().toDouble();
	}

	curFile.close();
}


QString BacklightDevice::name()
{
	return QFileInfo(devPath).fileName();
}


qreal BacklightDevice::currentBrightness()
{
	/* Brightness scaled to 1000 */
	QFile curFile(devPath + "/brightness");

	if (not curFile.open(QIODevice::ReadOnly))
	{
		return -1;
	}

	qreal curBrightness = QString::fromLocal8Bit(curFile.readAll()).simplified().toDouble();

	curFile.close();

	return curBrightness * 1000.0 / maxBrightness;
}


qint64 BacklightDevice::maximumBrightness()
{
	return (qint64)maxBrightness;
}


BacklightWidget::BacklightWidget(QWidget *parent) : QWidget(parent)
{
	session1 = new QDBusInterface(
		"org.freedesktop.login1",
		"/org/freedesktop/login1/session/self",
		"org.freedesktop.login1.Session",
		QDBusConnection::systemBus()
		);

	Q_FOREACH (QFileInfo blInfo, QDir("/sys/class/backlight/").entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries))
	{
		backlights << BacklightDevice(QFileInfo("/sys/class/backlight/" + blInfo.fileName()).symLinkTarget());
	}

	/* For devices which have led based backlighting */
	if (QFile::exists("/sys/class/leds/lcd-backlight"))
	{
		backlights << BacklightDevice("/sys/class/leds/lcd-backlight/");
	}

	maxBrightness = 255;            // So that the screen never goes to zero
	curBrightness = 1;              // So that the screen never goes to zero

	QLabel *lbl = new QLabel("BRIGHTNESS");
	lbl->setFont(QFont(font().family(), 11));
	lbl->setAlignment(Qt::AlignLeft);

	Q_FOREACH (BacklightDevice dev, backlights)
	{
		QSlider *slider = new QSlider(Qt::Horizontal, this);
		slider->setRange(1, 1000);
		connect(slider, &QSlider::sliderReleased, [ = ]() {
			changeBacklight(slider->value(), slider);
		});

		sliders << slider;
	}

	setCurrent();

	timer.start(1000, this);                     // 1 sec timer. Checks if any other process has changed the brightness behind the scenes.

	QVBoxLayout *lyt = new QVBoxLayout();
	lyt->setAlignment(Qt::AlignLeft);
	lyt->addWidget(lbl);

	/* Only one device, no name needed */
	if (backlights.count() == 1)
	{
		lyt->addWidget(sliders[0]);
	}

	/* Multiple devices, name them */
	else
	{
		for (int i = 0; i < backlights.count(); i++)
		{
			lyt->addWidget(new QLabel(backlights[i].name()));
			lyt->addWidget(sliders[i]);
		}
	}

	setLayout(lyt);
}


void BacklightWidget::setCurrent()
{
	for (int i = 0; i < backlights.count(); i++)
	{
		if (sliders[i]->isSliderDown())
		{
			continue;
		}

		qreal brightness = backlights[i].currentBrightness();

		if (brightness == -1)
		{
			CPrime::MessageEngine::showMessage(
				"cc.cubocore.CoreAction",
				"CoreAction",
				"Brightness Add-on",
				"Cannot get the current brightness of the screen. Default value taken as 100.");
			sliders[i]->setValue(400);

			continue;
		}

		sliders[i]->setValue(brightness);
	}
}


void BacklightWidget::changeBacklight(int value, QSlider *slider)
{
	int sliderIndex = -1;

    for (int i = 0; i < sliders.count(); i++)
    {
        if (slider == sliders[i])
        {
            sliderIndex = i;
            break;
        }
    }

    qint64 maxVal = backlights[sliderIndex].maximumBrightness();

    QDBusReply<void> reply = session1->call(
        "SetBrightness",                                // DBus Interface method
        "backlight",                                    // Subsystem {backlight or leds}
        backlights[sliderIndex].name(),                 // Device name
        ( uint )(value * maxVal / 1000.0)               // Brightness value
        );

    if (not reply.isValid())
    {
        qWarning() << "Error changing brightness:" << reply.error().message();
    }
}


void BacklightWidget::timerEvent(QTimerEvent *tEvent)
{
	if (tEvent->timerId() == timer.timerId())
	{
		/* Set the actual position of the slider */

		setCurrent();
	}

	QWidget::timerEvent(tEvent);
	tEvent->accept();
}


/* Name of the plugin */
QString BacklightPlugin::name()
{
	return "Backlight";
}


/* The plugin version */
QString BacklightPlugin::version()
{
	return QString(VERSION_TEXT);
}


/* The Widget hooks for menus/toolbars */
QWidget *BacklightPlugin::widget(QWidget *parent)
{
	return new BacklightWidget(parent);
}
