/*
	*
	* This file is a part of ShareIt wrapper.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>

#include <cprime/shareit.h>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	app.setOrganizationName("CuboCore");
	app.setApplicationName("ShareIT");
	app.setApplicationVersion(VERSION_TEXT);

	QCommandLineParser cliparser;
	cliparser.addHelpOption();         // Help
	cliparser.addVersionOption();      // Version

	cliparser.setApplicationDescription("A Simple wrapper for LibCPrime ShareIT to access it from system file manager.");
	cliparser.addOptions({
		{ { "share", "s" }, "Share files" },
	});
	cliparser.addPositionalArgument("file", "The file to which the screenshot is written.");

	cliparser.process(app);

	QStringList files = cliparser.positionalArguments();
	if (cliparser.isSet("share")) {
		if (!files.count()) {
			qDebug() << "No file given to share.";
			cliparser.showHelp();
			return 0;
		}

		ShareIT t(files, QSize(24,24), nullptr);
		t.resize(500,600);
		t.exec();
		return 0;
	} else {
		cliparser.showHelp();
		return 0;
	}

	return app.exec();
}
