# CoreToppings
Additional features, plugins, widgets etc for C Suite..

### Installation path
* CoreAction widgets - usr/lib/coreapps/plugins
* ShareIT plugins    - usr/lib/coreapps/shareit

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/coreapps/coretoppings/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)
* [DebPackages](https://gitlab.com/cubocore/wiki/-/tree/master/DebPackages)
* [Gentoo](https://gitweb.gentoo.org/repo/proj/guru.git/tree/gui-apps/coretoppings)

### Dependencies:
* qt6-base
* qt5-x11extras
* qt6-connectivity
* libpulse
* libdbusmenu-lxqt
* [libcprime](https://gitlab.com/cubocore/libcprime)

### Dependencies for media
#### For X11

* ffmpeg - screen/audio capture/recording
* v4l-utils - for webcam

#### For wayland (optional)

* grim - for screencapture
* wf-recorder - for screen recording

### Dependencies for media control
* playerctl

### Dependencies for rotation
* xorg-xrandr
* iio-sensor-proxy (optional) – for autorotation
* inotify-tools (optional) – for autorotation 

### Dependencies for networking switch
* bluez-utils - for bluetoothctl
* linux-utils - for rfkill
* networkmanager or connman - for wifi and hotspot

### Dependencies for qwickaccess
* redshift - for nightmode
* xorg-xinput - for disabling touchpad/touchscreen/keyboard etc.
* grep
* gawk
* polkit - for elevated privilage
* libnotify - for notifications

### Dependencies for leave
* systemd (optional)
* xdg-utils - for lockscreen

### Information
Please see the [Wiki page](https://gitlab.com/cubocore/wiki) for this info.
* Changes in latest release ([ChangeLog](https://gitlab.com/cubocore/wiki/-/blob/master/ChangeLog))
* Build from the source ([BuildInfo](https://gitlab.com/cubocore/wiki/blob/master/BuildInfo.md))
* Tested In ([Test System](https://gitlab.com/cubocore/wiki/blob/master/TestSystem))
* Known Bugs ([Current list of issues](https://gitlab.com/groups/cubocore/coreapps/-/issues))
* Help Us

### Feedback
* We need your feedback to improve the C Suite. Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/coreapps/-/issues).
  
  Or feel free to join and chat with us in IRC/Matrix #cubocore:matrix.org or [Element.io](https://app.element.io/#/room/#cubocore:matrix.org)
