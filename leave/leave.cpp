/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QMessageBox>
#include <QProcess>

#include "leave.h"
#include "ui_leave.h"

leave::leave(QWidget *parent): QWidget(parent), ui(new Ui::leave)
{
    ui->setupUi(this);
}

leave::~leave()
{
    delete ui;
}

void leave::on_toolButton_lockscreen_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/lockscreen.sh");
    proc.waitForFinished();

}

void leave::on_toolButton_logout_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Log Out?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/logout.sh");
        proc.waitForFinished();
    }
}

void leave::on_toolButton_suspend_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Suspend the Device?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/suspend.sh");
        proc.waitForFinished();
    }
}

void leave::on_toolButton_poweroff_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Power Off The device?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/poweroff.sh");
        proc.waitForFinished();
    }
}

void leave::on_toolButton_reboot_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Reboot The device?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/reboot.sh");
        proc.waitForFinished();
    }
}

void leave::on_toolButton_hibernate_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Hibernate The device?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/hibernate.sh");
        proc.waitForFinished();
    }
}

void leave::on_toolButton_rebbotuefi_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Reboot To UEFI?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/reboot-to-uefi.sh");
        proc.waitForFinished();
    }
}


void leave::on_toolButton_hybridsleep_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Hybrid Sleep The device?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/hybrid-sleep.sh");
        proc.waitForFinished();
    }
}

void leave::on_toolButton_suspenthenhibernate_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("leave");
    msgBox.setText("Do you want to Suspend Then Hibernate The device?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/coreapps/scripts/suspend-then-hibernate.sh");
        proc.waitForFinished();
    }
}
/* Name of the plugin */
QString leavePlugin::name()
{
    return "Leave";
}

/* The plugin version */
QString leavePlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *leavePlugin::widget(QWidget *parent)
{
    return new leave(parent);
}
