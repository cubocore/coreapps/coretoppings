/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <cprime/cplugininterface.h>

QT_BEGIN_NAMESPACE
namespace Ui {
    class media;
}
QT_END_NAMESPACE

class media : public QWidget {
    Q_OBJECT

public:
    media(QWidget *parent = nullptr);
    ~media();

private slots:
    void on_toolButton_camera_clicked();
    void on_toolButton_audiorecorder_clicked(bool checked);
    void on_toolButton_screenshot_clicked();
    void on_toolButton_screenrecorder_clicked(bool checked);
    void on_toolButton_screencam_clicked(bool checked);

private:
    Ui::media *ui;
};

class mediaPlugin : public WidgetsInterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID WIDGETS_PLUGININTERFACE)
    Q_INTERFACES(WidgetsInterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);
};
