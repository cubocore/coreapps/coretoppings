project( rotation )
cmake_minimum_required( VERSION 3.16 )

set( PROJECT_VERSION 5.0.0 )
set( PROJECT_VERSION_MAJOR 5 )
set( PROJECT_VERSION_MINOR 0 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )
add_compile_definitions(VERSION_TEXT="${PROJECT_VERSION}")

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )
set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_AUTOUIC ON )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

find_package( Qt6Core REQUIRED )
find_package( Qt6Gui REQUIRED )
find_package( Qt6Widgets REQUIRED )

set( rotation_HDRS

)

set( rotation_SRCS
	rotation.cpp
)

set( rotation_MOCS
	rotation.h
)

set( rotation_UIS
	rotation.ui
)

add_library( rotation MODULE ${rotation_SRCS} ${rotation_MOCS} ${rotation_UIS}  )
target_link_libraries( rotation  Qt6::Core Qt6::Gui Qt6::Widgets  cprime-widgets cprime-core )

install( TARGETS rotation LIBRARY DESTINATION lib/coreapps/plugins )
install( DIRECTORY scripts DESTINATION share/coreapps )
