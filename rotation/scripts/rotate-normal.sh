#!/bin/bash
kscreen-doctor output."$(kscreen-doctor -o | grep "primary" | awk '/Output/ { print $3 }')".rotation.normal
xrandr --output $(xrandr | egrep -o '^.+ connected' | cut -d " " -f 1) --rotate normal
touchscreenid=$(xinput --list | grep -i 'touchscreen' | grep -o 'id=[0-9]*' | sed 's/id=//')
xinput set-prop $touchscreenid 'Coordinate Transformation Matrix' 1 0 0 0 1 0 0 0 1;;
